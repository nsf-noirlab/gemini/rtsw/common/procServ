%{!?_pkgdocdir: %global _pkgdocdir %{_docdir}/%{name}-%{version}}

Summary: Process server with telnet console and log access
Name: procServ
Version: 2.8.0
Release: 10

# vendor/upstream git project
%define vendor_project https://github.com/ralphlange/procServ.git
# vendor git ref (tag or commit hash). Please keep in sync with 'Version', if possible!
%define vendor_ref cd68a34

License: GPLv3
URL: https://github.com/ralphlange/procServ
Source0: https://github.com/ralphlange/procServ/releases/download/v%{version}/procServ-%{version}.tar.gz
# Commenting out libtelnet-devel because no package existing
#BuildRequires: libtelnet-devel gcc-c++
Requires: python38
BuildRequires: asciidoc dblatex python38 automake autoconf gcc-c++

%description
procServ is a wrapper that starts an arbitrary command as a child process in
the background, connecting its standard input and output to a Unix domain
socket or a TCP port for telnet access.
It supports logging, child restart (manual or automatic on exit), and more.

procServ does not have the rich feature set of the screen utility,
but is intended to provide running a command in a system service style,
in a small, robust way.
Handling multiple users, authorization, authentication, central logging
is done best on a higher level, using a package like conserver.

For security reasons, procServ only accepts connections from localhost.


%package conserver
Summary: conserver auto-enabled systemd startup script
Requires: %{name} conserver conserver-client

%description conserver
The procServ-conserver package provides a systemd service script for
the conserver console server. It is configured to let conserver start
on system startup. This is wanted behavior to auto-setup containers with
softIOC instances.

%prep
%setup -q
git clone --recurse-submodules %{vendor_project} vendor_project
cd vendor_project
git checkout %{vendor_ref}
git submodule update --init --recursive
#disable generation of pdf and html docs becaus of missing 
#asciidoc-latex package in centos8/rocky8
git apply ../disable_html_pdf_docs.patch

%build
cd vendor_project

#regenerate configure 
make -f makefile

# change default configuration. Doc would not build.
%configure --docdir=%{_pkgdocdir} --enable-doc --enable-access-from-anywhere --with-systemd-utils PYTHON=/usr/bin/python3.8
make %{?_smp_mflags}

%install
# cd into the directory containing the vendor sources
cd vendor_project

rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
mkdir -p $RPM_BUILD_ROOT/etc/systemd/system

# install conserver systemd unit file and conserver configuration file
# to handle softIOC connections
# (our main purpose for conserver. We install it from procServ
# and not from conserver since conserver is provided by non-Gemini repos)
cp ../gem-files/conserver-softiocs.cf $RPM_BUILD_ROOT/etc/
cp ../gem-files/conserver.service $RPM_BUILD_ROOT/etc/systemd/system/

%post
mkdir -p /var/consoles

%post conserver
systemctl daemon-reload
systemctl restart conserver
systemctl enable conserver

%postun conserver
if [ $1 -eq 0 ]; then
	systemctl daemon-reload
fi

%files
%{_pkgdocdir}/
%{_bindir}/procServ
%{_bindir}/manage-procs
%{_bindir}/procServ-launcher
/usr/lib/python3.8/site-packages/procServUtils*
/usr/lib/systemd/system-generators/systemd-procserv-generator-system
/usr/lib/systemd/user-generators/systemd-procserv-generator-user
%{_mandir}/man1/procServ.1*
%{_mandir}/man1/manage-procs.1*

%files conserver
/etc/conserver-softiocs.cf
/etc/systemd/system/conserver.service

%changelog
* Sat Oct 08 2022 fkraemer <fkraemer@gemini.edu> 2.8.0-9
- reorganize project to fetch vendor code during build process: close #6, #7,
  #8
- updated gem-ci unstable/2022q4
- upgraded gem-ci for new tito builder and pipeline
- changed startup options of conserver for solving #3
- allow unencrypted connections for conserver service (add option -E)
- added 10.2.2.104 to the list of trusted gemini servers

* Thu Feb 17 2022 Felix Kraemer <felix.kraemer@noirlab.edu> 2.8.0-8
- fixed specfile regarding libdir
- fixed specfile regarding libdir
- fixed specfile regarding documentation
- changed specfile to provide conserver startup script in seperated package
- fixed specfile to restart conserver service
- added uninstall section to reload systemd services
- fixed bug in specfile
-  added conserver systemd unit file
- added conserver configuration file for softIOCs at Gemini
- update gem-ci
- Updated to latest gem-ci and branch stable/2021q4
- Changed to use gem-ci/testing/2021q4
- Update to gem-ci
- Updated gem-ci
- added gem-ci submodule and updated tito releasers.conf to be a symlink

